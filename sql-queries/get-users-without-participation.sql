SELECT ip.id_individu_pulsar, p.id_participant, ip.nom, ip.prenom, ca.adresse_courriel, ip.horodate_creation, ip.horodate_dern_modif, rip.valeur_chaine
FROM individus.individu_pulsar as ip
    INNER JOIN individus.compte_acces as ca ON ca.id_individu_pulsar = ip.id_individu_pulsar
    INNER JOIN individus.renseign_individu_pulsar  as rip ON rip.id_individu_pulsar = ip.id_individu_pulsar
    LEFT JOIN individus.participant as p ON p.id_individu_pulsar = ip.id_individu_pulsar
WHERE rip.id_attribut = 116 AND valeur_chaine='perspec' AND id_participant is null
ORDER BY horodate_creation DESC;