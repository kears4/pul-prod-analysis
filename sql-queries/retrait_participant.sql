/* SELECT Data script */
SELECT
    *
FROM
    individus.consent_participant as cp
WHERE 
    cp.id_participant IN (SELECT
        p.id_participant
    FROM
        individus.participant as p
    WHERE 
        p.id_individu_pulsar = (SELECT ca.id_individu_pulsar
        FROM individus.compte_acces as ca
        WHERE adresse_courriel = 'joseane.gilbert-moreau.1@ulaval.ca'));


/* Update data script */

UPDATE
    individus.consent_participant
SET
    indic_accept_consent = false,
    id_usager_dern_modif = 'kears4',
    horodate_dern_modif = '2019-12-09 10:00:00.000-00'
WHERE 
    id_participant IN (SELECT
        p.id_participant
    FROM
        individus.participant as p
    WHERE 
        p.id_individu_pulsar = (SELECT ca.id_individu_pulsar
        FROM individus.compte_acces as ca
        WHERE adresse_courriel = 'joseane.gilbert-moreau.1@ulaval.ca'));